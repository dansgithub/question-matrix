import React from 'react';
import RadioButton from "./RadioButton/RadioButton";
import Image from "./Image/Image";

const TableRow = (props) => {
    let columnIndex = props.columnIndex;

    return (
        <tr>
            <th className="Image">
                <Image imageCount={props.imageCount}/>
            </th>
            <th data-key={columnIndex} onBlur={props.onBlur} contenteditable="true">{props.children}</th>
            {props.columns.map((item, i) => (
                <th key={i}>
                    <RadioButton testProp={columnIndex}/>
                </th>
            ))}
        </tr>
    )
};

export default TableRow;
