import React, {Fragment} from 'react';
import './RadioButton.scss';

const RadioButton = (props) => {
    return (
        <Fragment>
            <input type="radio" id={props.testProp} name={props.testProp}/>
            <label htmlFor={props.testProp}/>
        </Fragment>
    )
};

export default RadioButton;
