import React, {Component, Fragment} from 'react';
import './Image.scss';

class Image extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedFile: null
        }
    }

    handleFileUpload = (e) => {
        this.setState({
            selectedFile: URL.createObjectURL(e.target.files[0])
        });

        this.props.imageCount();
    };

    render() {
        return (
            <Fragment>
                <input
                    hidden
                    onChange={this.handleFileUpload}
                    type="file"
                    ref={fileInput => this.fileInput = fileInput}
                />

                <img onClick={() => {
                    this.fileInput.click()
                }} className="Image" src={this.state.selectedFile}/>
            </Fragment>
        )
    }
}

export default Image;
