import React from 'react';
import './Dialog.scss';

const Dialog = (props) => {
    return (
        <div className="Dialog">
            <div className="Dialog-inner">
                <p>Please name your <strong>{props.placeholder}</strong></p>

                <input autoFocus
                       className="Dialog-input"
                       onKeyPress={props.onKeyPress}
                       placeholder={props.placeholder}
                       type="text"
                />
            </div>
        </div>
    )
};

export default Dialog;
