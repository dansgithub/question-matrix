import React from 'react';

const HeadContentWrapper = (props) => {
    return (
        <tr>
            <td>{null}</td>
            <td>{null}</td>

            {props.children}
        </tr>
    )
};

export default HeadContentWrapper;