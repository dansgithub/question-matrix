import React, {Component, Fragment} from 'react';
import './App.scss';

import TableRow from './components/TableRow';
import Dialog from "./components/Dialog/Dialog";
import HeadContentWrapper from "./components/HeadContentWrapper";
import Image from "./components/Image/Image";
import Button from "./components/Button/Button";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rows: [
                'ES6',
                'SCSS',
                'Gulp',
                'Node'
            ],
            columns: [
                'nah',
                'ok',
                'good',
                'nice'
            ],
            dialog: false,
            rowsOrCols: null,
            selectedFile: null,
            uploadedFiles: 0
        };
    }

    addRow = (e) => {
        this.openDialog('rows');
    };

    addColumn = (e) => {
        this.openDialog('columns');
    };

    openDialog(string) {
        const {dialog} = this.state;

        this.setState({
            dialog: !dialog,
            rowsOrCols: string
        });
    }

    removeColumn = (e) => {
        const {columns} = this.state;
        columns.splice(-1, 1);
        this.setState({columns});
    };

    removeRow = (e) => {
        const {rows} = this.state;
        rows.splice(-1, 1);
        this.setState({rows});
    };

    handleKeyPress = (e) => {
        if (e.key === 'Enter' && e.target.value !== '') {
            this.updateRowsOrCols(e);
        }
    };

    updateRowsOrCols(e) {
        const {columns, rows, dialog, rowsOrCols} = this.state;

        if (rowsOrCols === 'columns') {
            this.setState({columns: [...columns, e.target.value]});
        } else {
            this.setState({rows: [...rows, e.target.value]});
        }

        this.setState({dialog: !dialog});
        e.target.value = null;
    }

    handleColumnEditing = (e) => {
        const {columns} = this.state;

        if (e.target.innerHTML !== columns[e.target.dataset.key]) {
            columns[e.target.dataset.key] = e.target.innerHTML;
            this.setState({columns});
        }
    };

    handleRowEditing = (e) => {
        const {rows} = this.state;

        if (e.target.innerHTML !== rows[e.target.dataset.key]) {
            rows[e.target.dataset.key] = e.target.innerHTML;
            this.setState({rows});
        }
    };

    imageCount = (e) => {
        this.setState({uploadedFiles: this.state.uploadedFiles + 1});
    };

    render() {
        return (
            <div className="App">
                <section className="section">
                    <h2 className="editor-headline">Question Edition View</h2>

                    <div className="editor">
                        <div className="editor-table">
                            <table>
                                <thead>
                                <HeadContentWrapper>
                                    {this.state.columns.map((item, i) => (
                                        <td key={i}>
                                            <Image imageCount={this.imageCount}/>
                                        </td>
                                    ))}
                                </HeadContentWrapper>

                                <HeadContentWrapper onBlur={this.handleColumnEditing} columns={this.state.columns}>
                                    {this.state.columns.map((item, i) => (
                                        <td contenteditable="true" onBlur={this.handleColumnEditing} data-key={i}
                                            key={i}>{item}</td>
                                    ))}
                                </HeadContentWrapper>
                                </thead>

                                <tbody>
                                {this.state.rows.map((item, i) => (
                                    <TableRow
                                        imageCount={this.imageCount}
                                        onBlur={this.handleRowEditing}
                                        columnIndex={i}
                                        key={i}
                                        columns={this.state.columns}>{item}</TableRow>
                                ))}
                                </tbody>
                            </table>

                            <Button onClick={this.removeRow} text="–"/>
                            <Button onClick={this.addRow} text="✚"/>

                        </div>

                        <Button onClick={this.removeColumn} text="–"/>
                        <Button onClick={this.addColumn} text="✚"/>

                        {this.state.dialog &&
                        <Dialog onKeyPress={this.handleKeyPress} placeholder={this.state.rowsOrCols}/>}
                    </div>
                </section>

                <section className="section">
                    <aside>
                        <ul className="Legend">
                            <li className="Legend-item">rows: {this.state.rows.length}</li>
                            <li className="Legend-item">columns: {this.state.columns.length}</li>
                            <li className="Legend-item">uploaded files: {this.state.uploadedFiles}</li>
                            <li className="Legend-item">longest
                                row: {(this.state.rows.reduce((a, b) => a.length > b.length ? a : b, '')).length}</li>
                            <li className="Legend-item">longest
                                column: {(this.state.columns.reduce((a, b) => a.length > b.length ? a : b, '')).length}</li>
                        </ul>
                    </aside>
                </section>
            </div>
        );
    }
}

export default App;
